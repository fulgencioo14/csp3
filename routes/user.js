const express = require('express');
const router = express.Router();
const auth = require('../auth');
const UserController = require('../controllers/user');

// routes for user CRUD
router.post('/register', (req, res) => {
	UserController.register(req.body).then(result => res.send(result))
})

router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.getUserDetails(user.id).then(result => res.send(result))
})

router.post('/login', (req, res) => {
	UserController.login(req.body).then(result => res.send(result))
})

router.put('/update', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.update(req.body, user).then(result => res.send(result))
})

router.delete('/delete', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.deleteUser(user.id).then(result => res.send(result))
})

// routes for category CRUD
router.post('/add/category', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.addCategory(req.body, user.id).then(result => res.send(result))
})

router.get('/get/category', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.allCategory(user.id).then(result => res.send(result))
})

router.put('/update/category/:categoryID', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.updateCategory(req.body, user.id, req.params.categoryID).then(result => res.send(result))
})

router.delete('/delete/category/:categoryID', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.deleteCategory(user.id, req.params.categoryID).then(result => res.send(result))
})

router.post('/add/transaction', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.addTransaction(req.body, user.id).then(result => res.send(result))
})

router.get('/get/transaction', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.allTransaction(user.id).then(result => res.send(result))
})

router.put('/update/transaction/:transactionID', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.updateTransaction(req.body, user.id, req.params.transactionID).then(result => res.send(result))
})

router.delete('/delete/transaction/:transactionID', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.deleteTransaction(user.id, req.params.transactionID).then(result => res.send(result))
})

router.get('/income', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.filterIncome(user.id).then(result => res.send(result))
})

router.get('/expense', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.filterExpense(user.id).then(result => res.send(result))
})

router.post('/verify-google-id-token', async(req, res) => {
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

module.exports = router