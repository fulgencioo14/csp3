const jwt = require('jsonwebtoken')
const secret = process.env.SECRET

// create an AccessToken using JWT whenever a user is logged in
module.exports.createAccessToken = (userInfo) => {
	//_id, email, isAdmin
	const payload = {
		id: userInfo._id,
		email: userInfo.email	
}
	return jwt.sign(payload, secret, {})
	//return a jwt token, jwt.sign(payload, secret, {option})
}


//verify authenticity of JWT 
module.exports.verify = (req, res, next) => {
	// req.headers.authorization
	let token = req.headers.authorization
	if (typeof token !== 'undefined') {
		// return res.send({auth: 'success'})
		// return next() -- executes the next block of code in the method

		// return res.send(token.slice(7, token.length))
		// using JS string method slice() to obtain the token value form the authorization property of the header 

		token = token.slice(7, token.length)
		//jwt.verify(token, secret, callback function)
		return jwt.verify(token, secret, (err, data) => {
			// if an error was generated, token is NOT authentic, return auth failed
			//othewise proceed to the next statement
			return (err) ? res.send({auth: 'failed'}) : next()
		})
		// return next
	} else {
		return res.send({auth: 'failed'})
	}
}

// decode JWT to get the information from it.
module.exports.decode = (token) => {

	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length)
		//verify token first prior to decoding
		return jwt.verify(token, secret, (err, data) => {
			return (err) ? null : jwt.decode(token, {complete: true}).payload
		})
	} else {
		return null
	}
}

/*
	JWTs - it needs a privateKeyorSecretKey, payLoad, callback function (optional
		
		headers: {
			Content-Type: 'application/json'
			Authorization: 'Bearer <token>'
		}
	)
*/