const User = require('../models/user')
const bcrypt = require('bcrypt')
const {OAuth2Client} = require('google-auth-library')
const clientId = '366921916836-9j457122n2habn1897l3hj2pf8u0i7op.apps.googleusercontent.com'
const auth = require('../auth')


// users CRUD functionality
//create
const register = (registerInfo) => {
	return User.findOne({email: registerInfo.email}).then((user) => {
		if (user === null) {
			const newUser = new User({
				firstName: registerInfo.firstName,  
				lastName: registerInfo.lastName,
				email: registerInfo.email,
				password: bcrypt.hashSync(registerInfo.password, 10),
				loginType: 'manual'
			})
			return newUser.save().then((user, err) => {
				return (err) ? false : true
			})
			return user
		} else {
			if (user.loginType === 'google') {
				return {
					error: 'use-google-login'
				}
			} else {
				return {
					error: 'account-already-registered'
				}
			}
		}
	})
}

//retrieve 
const login = (loginInfo) => {
	return User.findOne({email: loginInfo.email}).then(returnedUser => {
		if (returnedUser === null) {
			return false
		} 

		const isPasswordMatched = bcrypt.compareSync(loginInfo.password, returnedUser.password)

	if (isPasswordMatched) {
		// generate a JWT and include it in the response to the client
		return {
			//pass in our user object as an argument to the createAccessToken() function so that the token payload can be created
			accessToken: auth.createAccessToken(returnedUser.toObject())
		}
	} else {
		return false
	}
})
}

//retrieve 
const getUserDetails = (userID) => {
	return User.findById(userID).then((returnedUser, err) => {
		if (err) {
			return false
		} else {
			returnedUser.password = undefined
			return returnedUser 
		}
	})
}

//update 
const update = (updateInfo, userPayload) => {
	let userUpdates = {
		firstName: updateInfo.firstName,  
		lastName: updateInfo.lastName,
		email: updateInfo.email,
		password: bcrypt.hashSync(updateInfo.password, 10),
	}
	if ((updateInfo.firstName !== '' && updateInfo.lastName !== '') && (updateInfo.email !== '' && updateInfo.password !== '' )) {

		if (updateInfo.email == userPayload.email) {
			return User.findByIdAndUpdate(userPayload.id, userUpdates).then((updatedUser, err) => {
				return (err) ? false : true
			})
		} else {
			return User.findOne({email: userUpdates.email}).then(user => {
				if (user === null) {
					return User.findByIdAndUpdate(userPayload.id, userUpdates).then((updatedUser, err) => {
						return (err) ? false : true
					})
				} else {
					return {
						error: 'Email is already in use.'
					}
				}
			}) 
		}
	} else {
		return User.findByIdAndUpdate(userPayload.id, userUpdates).then((updatedUser, err) => {
			return {
				error: 'All fields must have input.'
			}
		})	
	 }
}

//delete
const deleteUser = (userID) => {
	return User.findById(userID).then((user, err) => {
		if (user !== null) {
			user.remove()
			return true
		} else {
			return {
				error: 'Failure account deletion.'
			}
		}
	})
}
//end of users CRUD

//categories CRUD

//create category
const addCategory = (addCategoryInfo, userID) => {
	return User.findById(userID).then((user, err) => {
		const updates = {
			$push: {
				categories: [{
					name: addCategoryInfo.name,
					type: addCategoryInfo.type
				}]
			}
		}
	const duplicate = user.categories.find(category => category.name.toLowerCase() === addCategoryInfo.name.toLowerCase())
		if (duplicate) {
			return false
		} else {
			return User.updateOne({_id: userID}, updates).then((updates, err) => {
				return (err) ? false : true
			})
		}
	})	
}

//retrieve all categories
const allCategory = (userID) => {
	return User.findById(userID).then((user, err) => {
		return user.categories.map(categories => categories)
	})
}

//update category
const updateCategory = (updateCategoryInfo, userID, categoryID) => {

	return User.findById(userID).then((user, err) => {
	let isExisting = user.categories.find(category => 
			category._id == categoryID)
	let count=0;
	for (let i=0; i < user.categories.length; i++) {
		if (user.categories[i]._id == categoryID) {
			count++
			break;
		} else {
			count++
		}
	}
	let totalCount = count-1
	
	if (isExisting !== '') {
		return (updateCategoryInfo.name !== '' && updateCategoryInfo.type !== '') ? 
		User.findByIdAndUpdate(userID, { $set: {
				[`categories.${totalCount}.name`]: updateCategoryInfo.name,
				[`categories.${totalCount}.type`]: updateCategoryInfo.type
			}}).then((data, err) => {
				return (err) ? false : true })	
		 : {message: 'All fields must be filled out.'}	
	}
})
} 

//delete category
const deleteCategory = (userID, categoryID) => {
	return User.findById(userID).then((data, err) => {
		if (data.categories.length > 0) {
			data.categories.pull(categoryID)
			return data.save().then((data, err) => {
				return (err) ? false : true
			})
		}
	})	
}

// transactions CRUD
// add transaction
const addTransaction = (addTransactionInfo, userID) => {
	return User.findById(userID).then(user => {
		
		const updates = {
			categoryName: addTransactionInfo.categoryName,
			categoryType: addTransactionInfo.categoryType,
			description: addTransactionInfo.description,
			amount: addTransactionInfo.amount
		}
		user.transactions.push(updates)
		user.save()
		if (user.transactions.length > 0) {
		const expense = 'expense'
		const income = 'income'
		let expenseTransactions = user.transactions.filter(transactions => transactions.categoryType.toLowerCase() === expense)
		 expenseTransactions = expenseTransactions.map(expenses => expenses.amount)
		
		let incomeTransactions = user.transactions.filter(transactions => transactions.categoryType.toLowerCase() === income)
		incomeTransactions = incomeTransactions.map(income => income.amount) 
		let totalAmount = 0;	
			for (let i=0; i < expenseTransactions.length; i++) {
				totalAmount = totalAmount - expenseTransactions[i]
			}

			for (let i=0; i < incomeTransactions.length; i++) {
				totalAmount = totalAmount + incomeTransactions[i]
			}
		jsonTotalAmount = JSON.stringify(totalAmount)
		}
		return User.findByIdAndUpdate(userID,{ $set: {
			balanceAfterTransaction: jsonTotalAmount
		}}).then((data, err) => {
			return (err) ? false : true
		})
	})
}

// retrieve transactions
const allTransaction = (userID) => {
	return User.findById(userID).then((user, err) => {
		return user.transactions.map(transactions => transactions)
	})
}

// update transactions
const updateTransaction = (updateTransactionInfo, userID, transactionID) => {

	return User.findById(userID).then((user, err) => {
	let isExisting = user.transactions.find(transaction => 
			transaction._id == transactionID)
	let count=0;
	let i;
	for (i=0; i < user.transactions.length; i++) {
		if (user.transactions[i]._id == transactionID) {
			count++
			break;
		} else {
			count++
		}
	}
	let totalCount = count-1
	
	if (isExisting !== '') {
		if (updateTransactionInfo.categoryName !== '' && updateTransactionInfo.categoryType !== '' && updateTransactionInfo.description && updateTransactionInfo.amount) {
	
	 	user.transactions[i] = {
				dateAdded: user.transactions[i].dateAdded,
				_id: transactionID,
				categoryName: updateTransactionInfo.categoryName,
				categoryType: updateTransactionInfo.categoryType,
				description: updateTransactionInfo.description,
				amount: parseInt(updateTransactionInfo.amount)
		}
		user.save()

		if (user.transactions.length > 0) {
		const expense = 'expense'
		const income = 'income'
		let expenseTransactions = user.transactions.filter(transactions => transactions.categoryType.toLowerCase() === expense)
		 expenseTransactions = expenseTransactions.map(expenses => expenses.amount)
		
		let incomeTransactions = user.transactions.filter(transactions => transactions.categoryType.toLowerCase() === income)
		incomeTransactions = incomeTransactions.map(income => income.amount) 
		let totalAmount = 0;	
			for (let i=0; i < expenseTransactions.length; i++) {
				totalAmount = totalAmount - expenseTransactions[i]
			}

			for (let i=0; i < incomeTransactions.length; i++) {
				totalAmount = totalAmount + incomeTransactions[i]
			}
		jsonTotalAmount = JSON.stringify(totalAmount)
		}
		return User.findByIdAndUpdate(userID,{ $set: {
			balanceAfterTransaction: jsonTotalAmount
		}}).then((data, err) => {
			return (err) ? false : true
		})	
		} else {
			return {
				message: 'All fields must be filled out.'
			}
		} 	
	}
})
}

const deleteTransaction = (userID, transactionID) => {
	return User.findById(userID).then(user => {
		if (user.transactions.length > 0) {
		user.transactions.id(transactionID).remove()
		user.save()
		const expense = 'expense'
		const income = 'income'
		let expenseTransactions = user.transactions.filter(transactions => transactions.categoryType.toLowerCase() === expense)
		 expenseTransactions = expenseTransactions.map(expenses => expenses.amount)
		
		let incomeTransactions = user.transactions.filter(transactions => transactions.categoryType.toLowerCase() === income)
		incomeTransactions = incomeTransactions.map(income => income.amount) 
		let totalAmount = 0;	
			for (let i=0; i < expenseTransactions.length; i++) {
				totalAmount = totalAmount - expenseTransactions[i]
			}

			for (let i=0; i < incomeTransactions.length; i++) {
				totalAmount = totalAmount + incomeTransactions[i]
			}
		jsonTotalAmount = JSON.stringify(totalAmount)
		return User.findByIdAndUpdate(userID,{ $set: {
			balanceAfterTransaction: jsonTotalAmount
		}}).then((data, err) => {
			return (err) ? false : true
		})	
		} 
	})
} 

const filterIncome = (userID) => {
	return User.findById(userID).then(user => {
		return user.transactions.filter(transaction => transaction.categoryType.toLowerCase() === 'income')
	})
}

const filterExpense = (userID) => {
	return User.findById(userID).then(user => {
		return user.transactions.filter(transaction => transaction.categoryType.toLowerCase() === 'expense')
	})
}

const verifyGoogleTokenId = async (tokenId) => {
	const client = new OAuth2Client(clientId)
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	})


	if (data.payload.email_verified === true) {
		const newUser = new User({
					firstName: data.payload.given_name,
					lastName: data.payload.family_name,
					email: data.payload.email,
					loginType: 'google'
				})
	
	return User.findOne({email: data.payload.email}).then((user, err) => {
			return (err) ?
			false
			: 
			(user !== null) 
				?
				(user.loginType === 'google') 
					?
					{
						accessToken: auth.createAccessToken(user.toObject()),
						_id: user._id,
						isAdmin: user.isAdmin
					}
					: 
					{error: 'login-type-error'}
				: newUser.save().then((user, err) => {
					return {
						accessToken: auth.createAccessToken(user.toObject()),
						_id: user._id,
						isAdmin: user.isAdmin
					}
				})
					
		})
	}
}

module.exports = {
	register,
	login,
	getUserDetails,
	update,
	deleteUser,
	addCategory,
	allCategory,
	updateCategory,
	deleteCategory,
	addTransaction,
	allTransaction,
	updateTransaction,
	deleteTransaction,
	filterIncome,
	filterExpense,
	verifyGoogleTokenId
}