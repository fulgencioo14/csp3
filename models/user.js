const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	password: {
		type: String
	},
	loginType: {
		type: String,
		required: [true, 'Login type is required']
	},
	categories: [
		{
			name: {
				type: String
			},
			type: {
				type: String
			}
		}
	],
	transactions: [
		{
			categoryName: {
				type: String
			},
			categoryType: {
				type: String
			},
			description: {
				type: String
			},
			amount: {
				type: Number
			},
			dateAdded: {
				type: Date,
				default: new Date()
			},
		}
	],
	balanceAfterTransaction: {
		type: Number
		}

})

module.exports = mongoose.model('User', userSchema)