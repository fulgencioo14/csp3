const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')

//configure the dotenv package to access environment variable via the 'process.env' object
require('dotenv').config()
const PORT = process.env.PORT
const DB = process.env.MONGODB 


// mongoose connection
mongoose.connect(DB, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => {
	console.log('Connected to localhost database')
})

mongoose.connection.on('error', () => {
    console.log('Oops something went wrong with our MongoDB connection')
})

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())

// api routes
const userRoutes = require('./routes/user')

// endpoints
app.use('/api/users', userRoutes)


// server initialization
app.listen(PORT, () => {
	console.log(`API is now online on port = ${PORT}`)
})
